#!/usr/bin/python
import serial, time
from twisted.internet import reactor, protocol

class EchoServer(protocol.Protocol):
    """This is just about the simplest possible protocol"""

    def dataReceived(self, r):
        "As soon as any data is received, write it back."
        r = r.strip()
        if (r != "0"):
            self.s.setRTS(0)
            self.s.setDTR(0)
            self.factory.last = r            
        if (r == "0"):
            self.transport.write(self.factory.last)
        elif (r == "1"):
            self.s.setRTS(1)
        elif (r == "2"):
            self.s.setDTR(1)
        elif (r == "3"):
            self.s.setRTS(1)
            self.s.setDTR(1)
        else:
            self.s.setRTS(0)
            self.s.setDTR(0)
            self.factory.last = "0"

class EchoClient(protocol.Protocol):
    """Once connected, send a message, then print the result."""
    
    def connectionMade(self):
        self.transport.write("0")
    
    def dataReceived(self, data):
        "As soon as any data is received, write it back."
        v = int(data)
        v += 1
        self.transport.write(str(v))
        self.transport.loseConnection()

    def connectionLost(self, reason):
        print "connection lost"

class EchoFactory(protocol.ClientFactory):
    protocol = EchoClient

    def clientConnectionFailed(self, connector, reason):
        print "Connection failed - goodbye!"
        s = serial.Serial("/dev/ttyS0")
        s.setRTS(0)
        s.setDTR(0)

        factory = protocol.ServerFactory()
        factory.protocol = EchoServer
        factory.protocol.s = s
        factory.last = "0"
        reactor.listenTCP(8000,factory)
    
    def clientConnectionLost(self, connector, reason):
        print "Connection lost - goodbye!"
        reactor.stop()

def main():
    """This runs the protocol on port 8000"""
    f = EchoFactory()
    reactor.connectTCP("localhost", 8000, f)
    reactor.run()

# this only runs if the module was *not* imported
if __name__ == '__main__':
    main()